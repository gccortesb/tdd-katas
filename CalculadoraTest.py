__author__ = 'gloria'

import unittest
from Calculadora import Calculadora


class CalculadoraTestCase(unittest.TestCase):
    def test_sumar_vacia(self):

        self.assertEqual(Calculadora().sumar(""),0,"Cadena vacia")

    def test_sumar_cadenaConUnNumero(self):
        self.assertEqual(Calculadora().sumar("1"),1,"Un numero")
        self.assertEqual(Calculadora().sumar("2"),2,"Un numero")


if __name__ == '__main__':
    unittest.main()
